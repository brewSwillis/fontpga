# Fonts_MiSTer

A collections of fonts designed for the MiSTer FPGA project and Analogue consoles.

### Star City
A font inspired by old computer fonts and based on Gotham Light. A study in designing a proportional font for a monospaced format.<br />
![Star City](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/preview/Misc_Star_City.png)<br />
Download: [MiSTer](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/Misc_Star_City.pf) &middot; [Analogue](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/analogue/Misc_Star_City.fnt)

### DULLARD
Mortal Kombat font, based on the Genesis port.<br />
![DULLARD](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/preview/Console_Dullard.png)<br />
Download: [MiSTer](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/Console_Dullard.pf) &middot; [Analogue](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/analogue/Console_Dullard.fnt)

### Comic Italic
Italic font based on [Comic Neue](http://www.comicneue.com/). Analogue version preview.<br />
![Comic Italic](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/preview/Misc_Comic_Italic__Analogue.png)<br />
Download: [MiSTer](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/Misc_Comic_Italic.pf) &middot; [Analogue](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/analogue/Misc_Comic_Italic.fnt)

### Pocket Sans
Font used in the NeoGeo Pocket fighters. Maybe even the BIOS?<br />
![Pocket Sans](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/preview/NGPC_Pocket_Sans.png)<br />
Download: [MiSTer](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/NGPC_Pocket_Sans.pf) &middot; [Analogue](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/analogue/NGPC_Pocket_Sans.fnt)

### Flashback
Font from Flashback: The Quest for Identity (Genesis version).<br />
Download: [MiSTer](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/Console_Flashback.pf) &middot; [Analogue](https://gitlab.com/brewSwillis/fonts_mister/-/raw/master/analogue/Console_Flashback.fnt)

#### Difference Between Analogue and MiSTer fonts
The only difference (for now) is the copyright glyph is Mister-Kun or the Analogue logo for the respective systems (on certain fonts).